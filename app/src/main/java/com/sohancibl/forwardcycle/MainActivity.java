package com.sohancibl.forwardcycle;

import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.style.TtsSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private Button mbutton;
    private TextView textdata;
    CountDownTimer mTimer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mbutton = (Button) findViewById(R.id.button);
        textdata = (TextView) findViewById(R.id.textView);




        mbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   new JSONTask().execute("http://jsonparsing.parseapp.com/jsonData/moviesDemoItem.txt") ;
                Toast.makeText(getApplicationContext(), "Button Pressed", Toast.LENGTH_SHORT).show();
                startTimer();

            }
        });
    }

    public class JSONTask extends AsyncTask<String,String, String>{

        @Override
        protected String doInBackground(String... params) {

          //  Toast.makeText(getApplicationContext(), "JSONTask class", Toast.LENGTH_SHORT).show();
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            StringBuffer buffer;
            InputStream stream;

            try {
            //    Toast.makeText(getApplicationContext(), "JSONTask try entered", Toast.LENGTH_SHORT).show();
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                buffer = new StringBuffer();

                String line = "";
                while ((line = reader.readLine()) != null) {
            //        Toast.makeText(getApplicationContext(), "JSONTask while entered", Toast.LENGTH_SHORT).show();
                    buffer.append(line);

                }
                String finalString = buffer.toString();

                JSONObject parrentOdject = new JSONObject(finalString);
                JSONArray parrentArray = parrentOdject.getJSONArray("movies");
                JSONObject finalObject = parrentArray.getJSONObject(0);

                String movieName = finalObject.getString("movie");
                int year = finalObject.getInt("year");

                return " "+year;

            } catch (MalformedURLException e) {
            //    Toast.makeText(getApplicationContext(), "MalformedURLException", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            } catch (IOException e) {
           //     Toast.makeText(getApplicationContext(), "IOException", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            return  null;
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            textdata.setText(result);
          //  Toast.makeText(getApplicationContext(), "onPostExecute", Toast.LENGTH_SHORT).show();

        }
    }

    private void startTimer() {

         mTimer = new CountDownTimer(60000, 5000) {
            @Override
            public void onTick(long millisUntilFinished) {

                textdata.setText("JSON DATA");
                new JSONTask().execute("http://jsonparsing.parseapp.com/jsonData/moviesDemoItem.txt");
                Toast.makeText(getApplicationContext(), "Hit Server :)", Toast.LENGTH_SHORT).show();
            }
             @Override
             public void onFinish() {
                 //nothing to do man
             }

        };
        mTimer.start();
    }
}